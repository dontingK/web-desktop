package cn.donting.web.desktop.plugin.test;

import cn.donting.web.desktop.plugin.WapClasspathTask;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.file.FileCollection;
import org.gradle.api.tasks.JavaExec;
import org.springframework.boot.gradle.plugin.ResolveMainClassName;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;


/**
 * sys 开发run
 */
public class SysRunExec extends JavaExec {

    @Override
    public void exec() {
        System.out.println("run ........");
        super.exec();
    }

    public void buildAll() {
        Project project = getProject();
        Project rootProject = project.getRootProject();
        Set<Project> subProjects = rootProject.getSubprojects();
        ArrayList<Task> tasks = new ArrayList<>();
        for (Project subProject : subProjects) {
            Task build=null;
            try {
                 build = subProject.getTasks().getByName("wapClasspath");
            }catch (Exception ex){
            }
            if (build == null) {
                continue;
            }
            tasks.add(build);
        }
        dependsOn(tasks.toArray());
    }

    public void setRun(){
        Project project = getProject();
        ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
        FileCollection classpath = bootRunMainClassName.getClasspath();
        Set<File> files = classpath.getFiles();
        setClasspath(classpath);
        try {
            String mainClass = WapClasspathTask.springbootMainClass(project);
            setMain(mainClass);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

}

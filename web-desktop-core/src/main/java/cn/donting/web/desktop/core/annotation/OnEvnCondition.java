package cn.donting.web.desktop.core.annotation;

import cn.donting.web.desktop.core.evn.CoreProperties;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.io.File;

public class OnEvnCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        MergedAnnotations annotations = metadata.getAnnotations();
        MergedAnnotation<EvnCondition> evnConditionMergedAnnotation = annotations.get(EvnCondition.class);
        Evn value = evnConditionMergedAnnotation.getEnum("value", Evn.class);
        if (value.equals(CoreProperties.getMode())) {
            return true;
        } else {
            return false;
        }
    }
}

package cn.donting.web.desktop.sdk.web;

import cn.donting.web.desktop.top.AppDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * @author donting
 * 2021-05-30 下午2:34
 */
@Service
public class AppDispatcherServlet implements AppDispatcher {
    @Autowired
    private DispatcherServlet dispatcherServlet;

    private boolean dispatcherServletIsInit=false;

    @Override
    public void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException {
        if(!dispatcherServletIsInit){
            initDispatcherServlet();
        }
        dispatcherServlet.service(httpRequest,httpResponse);
    }


   private synchronized void  initDispatcherServlet() throws ServletException {
       if(!dispatcherServletIsInit){
           dispatcherServlet.init(AppPostProcessor.appServletConfig);
           dispatcherServletIsInit=true;
       }
   }
}

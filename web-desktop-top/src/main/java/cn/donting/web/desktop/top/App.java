package cn.donting.web.desktop.top;


import java.lang.annotation.*;

/**
 * @author donting
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface App {
    String name();
    String icon() default "app.png";
    String version();
    int numberVersion();
    Os[] sys() default {Os.WIN,Os.LINUX,Os.MAC};
    /**
     * app 主页地址
     * @return
     */
    String index();
    AppType type() default AppType.App;
}

package cn.donting.web.desktop.file.resources.vo;

import cn.hutool.core.io.FileUtil;
import lombok.Getter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
public class FileVo {
    private String name;
    private boolean isFile;
    private String path;
    private String extName;
    private long size;

    public FileVo(File file) {
        name=file.getName();
        isFile=file.isFile();
        path=file.getPath();
        extName= FileUtil.extName(file);
        size=file.length();
    }

    public static List<FileVo> build(File file){
        ArrayList<FileVo> fileVos=new ArrayList<>();
        if(file.isFile()){
            fileVos.add(new FileVo(file));
            return fileVos;
        }
        File[] files = file.listFiles();
        for (File file1 : files) {
            fileVos.add(new FileVo(file1));
        }
        return fileVos;
    }

}

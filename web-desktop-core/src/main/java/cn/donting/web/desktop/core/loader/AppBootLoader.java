package cn.donting.web.desktop.core.loader;

import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.entity.AppInfo;
import org.springframework.boot.loader.archive.JarFileArchive;

import java.io.File;

public class AppBootLoader extends AppLoader {
    @Override
    public boolean isLoader(File file) {
        return false;
    }



    @Override
    public Application loader(File file, AppInfo appInfo) throws Exception {
        JarFileArchive jarFileArchive = new JarFileArchive(file);
        BootLauncher bootLauncher = new BootLauncher(jarFileArchive);
        AppURLClassLoader appClassLoader = bootLauncher.createAppClassLoader();
        String mainClass = bootLauncher.getMainClass();
        return creatApp(mainClass, appClassLoader,  appInfo);
    }
}

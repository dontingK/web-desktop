package cn.donting.app;

import cn.donting.web.desktop.sdk.web.user.UserSpace;
import cn.donting.web.desktop.top.file.FileOpen;
import cn.donting.web.desktop.top.file.FileRegister;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author donting
 * 2021-05-30 下午4:27
 */
@RestController
@Slf4j
public class Conter  {

    @Autowired
    HttpServletRequest request;

    @Autowired
    HttpServletResponse response;

    @Autowired
    UserSpace userSpace;

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String fileName = file.getOriginalFilename();
        log.info("上传文件名：" + fileName);
        log.info("上传成功");
        return "上传成功";

    }


    @GetMapping("/test")
    public String test() {

        return userSpace.getUserSpace().toString();
    }


}

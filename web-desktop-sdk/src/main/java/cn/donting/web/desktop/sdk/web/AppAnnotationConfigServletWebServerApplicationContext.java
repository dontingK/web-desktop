package cn.donting.web.desktop.sdk.web;

import cn.donting.web.desktop.top.AppContext;
import cn.donting.web.desktop.top.AppDispatcher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * @author donting
 * 2021-05-30 下午3:25
 */
public class AppAnnotationConfigServletWebServerApplicationContext extends AnnotationConfigServletWebServerApplicationContext implements AppContext {
    public AppAnnotationConfigServletWebServerApplicationContext() {
    }

    public AppAnnotationConfigServletWebServerApplicationContext(DefaultListableBeanFactory beanFactory) {
        super(beanFactory);
    }



    public AppAnnotationConfigServletWebServerApplicationContext(Class<?>... annotatedClasses) {
        super(annotatedClasses);
    }

    public AppAnnotationConfigServletWebServerApplicationContext(String... basePackages) {
        super(basePackages);
    }

    @Override
    public void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException {
        AppDispatcherServlet bean = getBean(AppDispatcherServlet.class);
        bean.doService(httpRequest, httpResponse);
    }

    @Override
    public String getProperties(String key) {
        return getEnvironment().getProperty(key);
    }
}

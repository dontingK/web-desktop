package cn.donting.web.desktop.sys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
/**
 * @author donting
 */
@SpringBootApplication(scanBasePackages = {"cn.donting.web.desktop.*"})
@EnableJpaRepositories("cn.donting.web.desktop.*")
@EntityScan("cn.donting.web.desktop.*")
@ServletComponentScan("cn.donting.web.desktop.*")
@EnableWebSocket
public class WebDesktopSysApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        ClassLoader classLoader = WebDesktopSysApplication.class.getClassLoader();
        SpringApplication.run(WebDesktopSysApplication.class, args);
    }

    /**
     * 重写configure
     *
     * @param builder
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(WebDesktopSysApplication.class);
    }
}

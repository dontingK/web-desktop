package cn.donting.web.desktop.core.configuration;


import cn.donting.web.desktop.core.app.AppManager;
import cn.donting.web.desktop.core.app.FileSysManger;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Configuration
public class StaticFileConfiguration implements WebMvcConfigurer {
    public static final String FILE_ICON_URL="/sys/file/icon/";
    public static final String APP_ICON_URL="/sys/app/icon/";
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(FILE_ICON_URL+"*").addResourceLocations("file:"+ FileSysManger.FILE_ICON_PATH+ File.separator);
        registry.addResourceHandler(APP_ICON_URL+"*").addResourceLocations("file:"+ AppManager.APP_ICON+ File.separator);
    }
}

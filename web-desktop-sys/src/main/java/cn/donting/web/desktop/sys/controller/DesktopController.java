package cn.donting.web.desktop.sys.controller;

import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.app.DesktopManager;
import cn.donting.web.desktop.top.interfaces.DesktopResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/sys/desktop")
public class DesktopController {

    @Autowired
    DesktopManager desktopManager;

    @RequestMapping("/appSdk.js")
    public void appSdk(HttpServletResponse response) throws Exception {
        Application nowDesktop = desktopManager.getNowDesktop();
        if (nowDesktop == null) {
            response.sendError(500, "getNowDesktop is null");
            return;
        }
        DesktopResources bean = nowDesktop.getAppContext().getBean(DesktopResources.class);
        response.sendRedirect(bean.webAppSdkUrl()+"?appId="+nowDesktop.getId());
    }

}

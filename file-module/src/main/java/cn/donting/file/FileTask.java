package cn.donting.file;

import cn.hutool.core.io.unit.DataSize;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=true)
public abstract class FileTask extends Thread {

    protected FileAction fileAction;
    protected DataSize MB_1 = DataSize.parse("1MB");
    protected int fileNums = 0;
    protected long countSize = 0;
    protected long opSize = 0;
    protected long opNums = 0;
    protected String nowDes = "";
    protected long beginTime;
    protected long endTime;
    protected String errorMsg;
    protected String taskId = ++FileTask.autoTaskId + "";

    protected boolean cancel = false;

    private static Long autoTaskId = 0L;

    public FileTask(FileAction fileAction) {
        this.fileAction = fileAction;
    }

    @Override
    public synchronized void start() {
        beginTime = System.currentTimeMillis();
        super.start();
    }

    /**
     * @param file
     * @param reverse true 重里到外 false 从外到里
     * @return
     */
    public List<File> loopFile(String file, boolean reverse) {
        nowDes = "开始扫描文件.....";
        return loopFile(new File(file), reverse);
    }

    public void cancel() {
        if (!cancel) {
            cancel = true;
            endTime=System.currentTimeMillis();
        }
    }

    private List<File> loopFile(File file, boolean reverse) {
        ArrayList<File> arrayList = new ArrayList<>();
        if (!reverse) {
            arrayList.add(file);
        }
        fileNums++;
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File file1 : files) {
                arrayList.addAll(loopFile(file1, reverse));
            }
        }else{
            countSize += file.length();
        }
        if (reverse) {
            arrayList.add(file);
        }
        return arrayList;
    }

    public String getTaskId() {
        return taskId;
    }

    public static void main(String[] args) {
        FileAction fileAction = new FileAction();
        fileAction.setSrcPaths(new String[]{"/Users/donting/工作空间/web-desktop/sys/users/admin/desktop/node_modules"});
        FileTask fileTask = new FileDeleteTask(fileAction);
        fileTask.start();



    }
}

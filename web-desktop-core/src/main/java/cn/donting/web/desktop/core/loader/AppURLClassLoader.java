package cn.donting.web.desktop.core.loader;

import cn.donting.web.desktop.core.entity.AppInfo;
import cn.donting.web.desktop.top.App;

import java.net.URL;
import java.net.URLClassLoader;

public class AppURLClassLoader extends URLClassLoader {

    private String name;
    private App app;
    private AppInfo appInfo;

    public AppURLClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        //TODO: 将来的权限检查
        return super.loadClass(name);
    }

    public  void setName(String name) {
        this.name = name;
    }

    public final void setApp(App app){
        if(this.app==null){
            this.app=app;
        }
    }

    public void setAppInfo(AppInfo appInfo) {
        if(this.app==null){
            this.appInfo = appInfo;
        }
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public final App getApp(){
        return this.app;
    }
}

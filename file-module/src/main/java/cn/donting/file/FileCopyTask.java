package cn.donting.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

//@Slf4j
public class FileCopyTask extends FileTask {
    private static final Log log = LogFactory.get();
    private final boolean move;
    private final String  type;

    public FileCopyTask( FileAction fileAction) {
        super(fileAction);
        move=false;
        type="移动";
    }



    public FileCopyTask(FileAction fileAction, boolean move) {
        super(fileAction);
        this.move = move;
        if (move) {
            type="移动";
        }else{
            type="复制";
        }
    }

    @Override
    public void run() {
        try {
            beginTime = System.currentTimeMillis();
            String[] srcPaths = fileAction.getSrcPaths();
            //需要复制的文件列表
            ArrayList<FileCopyWrapper> fileCopyWrappers = new ArrayList();
            for (String srcPath : srcPaths) {
                FileCopyWrapper fileCopyWrapper = new FileCopyWrapper(srcPath, loopFile(srcPath,false));
                fileCopyWrapper.files.add(new File(srcPath));
                fileCopyWrappers.add(fileCopyWrapper);
                if(cancel){
                    return;
                }
            }
            for (FileCopyWrapper fileCopyWrapper : fileCopyWrappers) {
                List<File> files = fileCopyWrapper.files;
                String parent = new File(fileCopyWrapper.src).getParent();
                if(cancel){
                    return;
                }
                for (File file : files) {
                    if(cancel){
                        return;
                    }
                    String dest = file.getPath().substring(parent.length());
                    dest =fileAction.getDestPath() + dest;
                    File destFile = new File(dest);
                    nowDes = "正在"+type+"："+file.getPath();
                    if (destFile.exists()) {
                        log.warn("file["+type+"]:文件以存在：{}", dest);
                    }

                    if (file.isDirectory()) {
                        new File(dest).mkdirs();
                    } else {
                        if (file.length() > MB_1.toBytes()) {
                            //流复制
                            FileChannel srcChannel = new FileInputStream(file).getChannel();
                            FileChannel desChannel = new FileOutputStream(destFile).getChannel();
                            ByteBuffer buffer = ByteBuffer.allocateDirect(1024*2);
                            int length ;
                            while ((length = srcChannel.read(buffer)) != -1) {
                                buffer.flip();
                                desChannel.write(buffer);
                                opSize += length;
                                buffer.clear();
                                if(cancel){
                                    srcChannel.close();
                                    desChannel.close();
                                    destFile.delete();
                                    return;
                                }
                            }
                            srcChannel.close();
                            desChannel.close();
                            if(move){
                                file.delete();
                            }
                        } else {
                            if(move){
                                FileUtil.move(file,new File(dest),true);
                            }else{
                                FileUtil.copy(file.getPath(), dest, true);
                            }
                            opSize += file.length();
                        }
                    }
                    opNums++;
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            nowDes = type+"失败：" + ex.getMessage();
            errorMsg = type+"失败：" + ex.getMessage();
        }
        endTime = System.currentTimeMillis();
    }


    public static void main(String[] args) {
        FileCopyTask fileCopyTask = new FileCopyTask(null);
        List<File> files = fileCopyTask.loopFile("D:\\新建文件夹\\web-desktop\\web-desktop-desktop\\desktop-ui",false);
        System.out.println();
    }


}

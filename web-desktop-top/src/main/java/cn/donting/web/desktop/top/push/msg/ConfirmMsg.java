package cn.donting.web.desktop.top.push.msg;

import lombok.Getter;

@Getter
public class ConfirmMsg {
    private String title;
    private String msg;
    public ConfirmMsg(String title, String msg) {
        this.title = title;
        this.msg = msg;
    }
}

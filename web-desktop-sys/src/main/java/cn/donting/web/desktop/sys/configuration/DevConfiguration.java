package cn.donting.web.desktop.sys.configuration;

import cn.donting.web.desktop.core.annotation.Evn;
import cn.donting.web.desktop.core.annotation.EvnCondition;
import cn.donting.web.desktop.core.app.UserManager;
import cn.donting.web.desktop.sys.service.UserCenter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
@EvnCondition(Evn.DEV)
@Slf4j
public class DevConfiguration implements ApplicationRunner {


    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}

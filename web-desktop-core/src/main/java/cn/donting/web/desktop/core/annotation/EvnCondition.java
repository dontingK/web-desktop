package cn.donting.web.desktop.core.annotation;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnEvnCondition.class)
public @interface EvnCondition {
    Evn value();
}

package cn.donting.web.desktop.servlet;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.session.Session;
import org.eclipse.jetty.server.session.SessionData;
import org.eclipse.jetty.server.session.SessionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author donting
 * 2021-06-09 下午11:07
 */
@Slf4j
public class AppSession extends Session {
    public AppSession(SessionHandler handler, HttpServletRequest request, SessionData data) {
        super(handler, request, data);
    }

    public AppSession(SessionHandler handler, SessionData data) {
        super(handler, data);
    }

    @Override
    public void setAttribute(String name, Object value) {
        log.debug("setAttribute  ThreadContext.getAppId():[{}]",ThreadContext.getAppId());
        name=ThreadContext.getAppId()+":"+name;
        log.debug("setAttribute:[{}]",name);
        super.setAttribute(name, value);
    }

    @Override
    public Object getAttribute(String name) {
        log.debug("getAttribute  ThreadContext.getAppId():[]"+ThreadContext.getAppId());
        name=ThreadContext.getAppId()+":"+name;
        log.debug("getAttribute:[{}]",name);
        return super.getAttribute(name);
    }


}

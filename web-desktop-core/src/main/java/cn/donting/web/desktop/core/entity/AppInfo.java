package cn.donting.web.desktop.core.entity;


import cn.donting.web.desktop.top.App;
import cn.donting.web.desktop.top.AppType;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class AppInfo {
    @Id
    @GeneratedValue
    private Long appId;

    @Column
    private String name;
    @Column
    private String version;
    @Column
    private Integer numberVersion;
    @Column
    private Date installTime;
    @Column
    private String fileName;
    @Column
    private String index;
    @Column
    private String icon;
    @Column
    private AppType type;
    @Column(unique = true)
    private String appKey;

    public static AppInfo buildInApp(App app) {
        AppInfo appInfo = new AppInfo();
        appInfo.name = app.name();
        appInfo.version = app.version();
        appInfo.numberVersion = app.numberVersion();
        appInfo.index = app.index();
        appInfo.type = app.type();
        return appInfo;
    }

    public static AppInfo sys() {
        AppInfo appInfo = new AppInfo();
        appInfo.appId = 0L;
        appInfo.name = "sys";
        appInfo.version = "0.1";
        appInfo.numberVersion = 1;
        appInfo.index = "/";
        return appInfo;
    }

    public String toString() {
        return "[" + name + ":" + appId + "]";
    }
}

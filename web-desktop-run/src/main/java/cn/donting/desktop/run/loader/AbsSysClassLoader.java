package cn.donting.desktop.run.loader;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author donting
 * 2021-05-29 下午2:11
 */
public abstract class AbsSysClassLoader extends URLClassLoader {
    public AbsSysClassLoader(URL[] urls) {
        super(urls);
    }

   public abstract Class<?> getMainClass() throws ClassNotFoundException;
}

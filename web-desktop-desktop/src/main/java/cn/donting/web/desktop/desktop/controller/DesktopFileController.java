package cn.donting.web.desktop.desktop.controller;


import cn.donting.file.*;
import cn.donting.file.vo.FileTaskVo;
import cn.donting.web.desktop.desktop.domain.ResponseEntity;
import cn.donting.web.desktop.desktop.vo.FileVo;
import cn.donting.web.desktop.sdk.web.user.UserSpace;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author donting
 */
@RestController
@RequestMapping("/desktop")
@Slf4j
public class DesktopFileController {

    @Autowired
    UserSpace userSpace;
    @Autowired
    FileTaskManger taskManger;


    @GetMapping("/files")
    public ResponseEntity<ArrayList<FileVo>> files() {
        File desktop = userSpace.getDesktop();
        File[] files = desktop.listFiles();
        ArrayList<FileVo> vos = new ArrayList<>(files.length);
        for (File file : files) {
            vos.add(FileVo.build(file));
        }
        return ResponseEntity.ok(vos);
    }

    /**
     * 重命名
     *
     * @param filePath
     * @param newName
     * @return
     */
    @PutMapping("/file/rename")
    public ResponseEntity rename(String filePath, String newName) {
        try {
            File file = new File(filePath);
            File newFile = new File(file.getPath() + File.separator + newName);
            return ResponseEntity.ok(file.renameTo(newFile));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.error(e.getMessage());
        }
    }

    /**
     * 创建文件
     *
     * @param name
     * @return
     */
    @PostMapping("/file")
    public ResponseEntity file(String name) {
        File file = new File(userSpace.getDesktop() + File.separator + name);
        if (file.exists()) {
            return ResponseEntity.error("文件以存在");
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.error(name + ":文件创建失败");
        }
        return ResponseEntity.ok(null);
    }

    /**
     * 创建文件夹
     *
     * @param name
     * @return
     */
    @PostMapping("/dir")
    public ResponseEntity dir(String name) {
        File file = new File(userSpace.getDesktop() + File.separator + name);
        if (file.exists()) {
            return ResponseEntity.error("文件已存在");
        }
        boolean mkdirs = file.mkdirs();
        return ResponseEntity.ok(mkdirs);
    }

    /**
     * 删除文件
     *
     * @param filePath
     * @return
     */
    @DeleteMapping("/files")
    public ResponseEntity files(@RequestBody String[] filePath) throws InterruptedException {
        FileAction fileAction = new FileAction();
        fileAction.setSrcPaths(filePath);
        FileDeleteTask fileDeleteTask = new FileDeleteTask(fileAction);
        fileDeleteTask.start();
        Thread.sleep(500);
        if (fileDeleteTask.getEndTime() != 0) {
            return ResponseEntity.ok("删除成功");
        } else {
            taskManger.push(fileDeleteTask);
            return ResponseEntity.ok(201, fileDeleteTask.getTaskId());
        }

    }

    @PostMapping("/files/move")
    public ResponseEntity filesMove(@RequestBody FileAction fileAction) throws InterruptedException {
        FileMoveTask fileMoveTask = new FileMoveTask(fileAction);
        fileMoveTask.start();
        Thread.sleep(800);
        if (fileMoveTask.getEndTime() != 0) {
            return ResponseEntity.ok("移动成功");
        } else {
            taskManger.push(fileMoveTask);
            return ResponseEntity.ok(201, fileMoveTask.getTaskId());
        }
    }

    @PostMapping("/files/copy")
    public ResponseEntity filesCopy(@RequestBody FileAction fileAction) throws InterruptedException {
        FileCopyTask fileCopyTask = new FileCopyTask(fileAction);
        fileCopyTask.start();
        Thread.sleep(800);
        if (fileCopyTask.getEndTime() != 0) {
            return ResponseEntity.ok("移动成功");
        } else {
            taskManger.push(fileCopyTask);
            return ResponseEntity.ok(201, fileCopyTask.getTaskId());
        }
    }

    @GetMapping("/file/task/{taskId}")
    public ResponseEntity fileTask(@PathVariable String taskId) {
        FileTask fileTask = taskManger.getFileTask(taskId);
        FileTaskVo fileTaskVo=new FileTaskVo(fileTask);
        return ResponseEntity.ok(fileTaskVo);
    }

    @DeleteMapping("/file/task/{taskId}")
    public ResponseEntity cancel(@PathVariable String taskId) {
        FileTask fileTask = taskManger.getFileTask(taskId);
        fileTask.cancel();
        FileTaskVo fileTaskVo=new FileTaskVo(fileTask);
        return ResponseEntity.ok(fileTaskVo);
    }
    @PostMapping("/uploads")
    public ResponseEntity upload(MultipartFile[] multipartFiles, String prentFilePath) {
        int success=0;
        try {
            File prentFile = new File(prentFilePath);
            for (MultipartFile multipartFile : multipartFiles) {
                File src = multipartFile.getResource().getFile();
                String name = multipartFile.getName();
                String targetFileName = cn.donting.file.util.FileUtil.fileName(name, prentFile);
                File targetFile=new File(prentFile.getPath()+File.separator+targetFileName);
                targetFile.createNewFile();
                cn.hutool.core.io.FileUtil.move(src,prentFile,false);
                success++;
            }
            return ResponseEntity.ok(null);
        } catch (IOException e) {
            log.error(e.getMessage(),e);
            return ResponseEntity.error("上传失败:成功["+success+"]");
        }
    }

}

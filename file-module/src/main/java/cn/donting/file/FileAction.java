package cn.donting.file;

import lombok.Data;

/**
 * 文件复制 body
 */
@Data
public class FileAction {
    /**
     * 源文件路径
     */
    String[] srcPaths;
    /**
     * 目标文件夹
     */
    String destPath;
}

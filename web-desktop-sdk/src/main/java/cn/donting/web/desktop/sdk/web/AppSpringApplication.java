package cn.donting.web.desktop.sdk.web;

import cn.donting.web.desktop.top.AppContext;
import org.springframework.boot.ApplicationContextFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author donting
 * 2021-05-30 下午3:18
 */
public class AppSpringApplication extends SpringApplication {

    public AppSpringApplication(Class<?>... primarySources) {
        super(primarySources);
        setApplicationContextFactory(new AppApplicationContextFactory());
    }

    @Override
    protected ConfigurableApplicationContext createApplicationContext() {
        return super.createApplicationContext();
    }

    public static AppContext runApp(Class<?> primarySource, String... args) {
        AppSpringApplication appSpringApplication = new AppSpringApplication(primarySource);
        ConfigurableApplicationContext run = appSpringApplication.run(args);
        return (AppContext)run;
    }

    public static int stopApp(AppContext appContext){
       return SpringApplication.exit((ApplicationContext) appContext);
    }


    private static class  AppApplicationContextFactory implements ApplicationContextFactory {

        @Override
        public ConfigurableApplicationContext create(WebApplicationType webApplicationType) {
            return new AppAnnotationConfigServletWebServerApplicationContext();
        }
    }

}

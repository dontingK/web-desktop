package cn.donting.web.desktop.core.configuration;

import cn.donting.web.desktop.core.annotation.Evn;
import cn.donting.web.desktop.core.annotation.EvnCondition;
import cn.donting.web.desktop.core.loader.AppBootLoader;
import cn.donting.web.desktop.core.loader.AppDevLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoaderAutoConfiguration {

    @Bean
    public AppBootLoader appBootLoader() {
        return new AppBootLoader();
    }
    @Bean
    @EvnCondition(Evn.DEV)
    public AppDevLoader appDevLoader(){
        return new AppDevLoader();
    }
}

//package cn.donting.web.desktop.core.app;
//
//import cn.donting.web.desktop.core.app.interfaces.UserCenter;
//import cn.donting.web.desktop.core.domain.TaskEntity;
//import cn.donting.web.desktop.core.entity.AppInfo;
//import cn.donting.web.desktop.core.entity.User;
//import cn.donting.web.desktop.core.exception.TaskException;
//import cn.donting.web.desktop.core.loader.AppURLClassLoader;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//import org.springframework.stereotype.Service;
//
//import java.util.*;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ThreadPoolExecutor;
//
////TODO:2.0
//@Service
//@Slf4j
//public class TaskManager implements TaskCenter {
//
//    @Autowired
//    @Qualifier("appManager")
//    AppManager appManager;
//    @Autowired
//    UserCenter userCenter;
//
//    private final Map<Long, TaskEntity> tasks = new ConcurrentHashMap<Long, TaskEntity>();
//    private final  ThreadPoolTaskExecutor executor;
//
//
//
//    public TaskManager() {
//        TaskContext.setTaskCenter(this);
//        executor = new ThreadPoolTaskExecutor();
//        executor.setCorePoolSize(5);//核心池大小
//        executor.setMaxPoolSize(20);//最大线程数
//        executor.setQueueCapacity(200);//队列程度
//        executor.setKeepAliveSeconds(1000);//线程空闲时间
//        executor.setThreadNamePrefix("tsak");//线程前缀名称
//        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());//配置拒绝策略
//    }
//
//
//
//
//
//    /**
//     * @param task
//     * @return
//     * @throws TaskException
//     */
//    @Override
//    public synchronized Long submit(Task task) throws TaskException {
//        AppURLClassLoader classLoader = (AppURLClassLoader) task.getClass().getClassLoader();
//        Long appId = classLoader.getAppInfo().getAppId();
//        User loginUser = userCenter.getLoginUser();
//        if (loginUser == null) {
//            throw new TaskException("登录用户为 null");
//        }
//        AppInfo app = appManager.getAppInfo(appId);
//        TaskEntity taskEntity = new TaskEntity(task, app, loginUser);
//        tasks.put(taskEntity.getTaskId(), taskEntity);
//        try {
//            executor.submit(task);
//        }catch (Exception exception){
//            log.error(exception.getMessage(),exception);
//            throw exception;
//        }
//        return taskEntity.getTaskId();
//    }
//
//    @Override
//    public Task cancel(Long id) throws TaskException{
//        TaskEntity taskEntity = tasks.get(id);
//        User loginUser = userCenter.getLoginUser();
//        if (!taskEntity.getUser().equals(loginUser)) {
//            log.warn("loginUser:{}",loginUser);
//            log.warn("tasklUser:{}",taskEntity.getUser());
//            throw new TaskException("用户 不一致");
//        }
//        taskEntity.getTask().cancel();
//        return taskEntity.getTask();
//    }
//
//    /**
//     * 获取用户的Task
//     * @param user
//     * @return
//     */
//    public List<TaskEntity> getUserTask(User user) {
//        List<TaskEntity> list = new ArrayList<>();
//        for (Map.Entry<Long, TaskEntity> taskEntityEntry : tasks.entrySet()) {
//            if(user.equals(taskEntityEntry.getValue().getUser())){
//                list.add(taskEntityEntry.getValue());
//            }
//        }
//        return list;
//    }
//
//
//}

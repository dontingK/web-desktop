module.exports = {
    lintOnSave: false, //是否开启eslint
    devServer: {
        port:8005,
        proxy: {
            '': {
                target: 'http://localhost:8080', //API服务器的地址
                ws: true, //代理websockets
                changeOrigin: true, // 是否跨域，虚拟的站点需要更管origin
                pathRewrite: {
                    // '^/api'是一个正则表达式，表示要匹配请求的url中，全部'http://localhost:8081/api' 转接为 http://localhost:8081/
                    // '^/api': '',
                }
            }
        },
    },
    pages: {
        'desktop.html': {
            // page 的入口
            entry: 'src/main.js',
            // 模板来源
            template: 'public/desktop.html',
            // 在 dist/index.html 的输出
            filename: 'desktop.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'desktop',
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            // chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
        'fileCopy.html': {
            // page 的入口
            entry: 'src/page/copyFile/main.js',
            // 模板来源
            template: 'public/window.html',
            // 在 dist/index.html 的输出
            filename: 'fileCopy.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'fileCopy Page',
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            // chunks: ['chunk-vendors', 'chunk-common', 'index']
        },

    }
}

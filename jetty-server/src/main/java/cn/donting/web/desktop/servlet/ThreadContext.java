package cn.donting.web.desktop.servlet;

/**
 * @author donting
 * 2021-06-14 上午11:09
 */
public class ThreadContext {

    private final static ThreadLocal<Long> APP_ID = new ThreadLocal<>();

    public static void setAppId(Long id) {
        APP_ID.set(id);
    }

    public static Long getAppId() {
        return APP_ID.get();
    }

    public static void removeAppId() {
        APP_ID.remove();
    }
}

package cn.donting.file;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//@Slf4j
public class FileDeleteTask extends FileTask {
    private static final Log log = LogFactory.get();

    public FileDeleteTask(FileAction fileAction) {
        super(fileAction);
    }

    @Override
    public void run() {
        log.info("开始删除：" + Arrays.toString(fileAction.srcPaths));
        try {
            beginTime = System.currentTimeMillis();
            String[] srcPaths = fileAction.getSrcPaths();
            //需要移动的文件列表
            ArrayList<FileCopyWrapper> fileCopyWrappers = new ArrayList();
            for (String srcPath : srcPaths) {
                FileCopyWrapper fileCopyWrapper = new FileCopyWrapper(srcPath, loopFile(srcPath, true));
                fileCopyWrapper.files.add(new File(srcPath));
                fileCopyWrappers.add(fileCopyWrapper);
            }
            if (cancel) {
                return;
            }
            for (FileCopyWrapper fileCopyWrapper : fileCopyWrappers) {
                if (cancel) {
                    return;
                }
                List<File> files = fileCopyWrapper.files;
                for (File file : files) {
                    if (cancel) {
                        return;
                    }
                    nowDes = "正在删除文件:" + file.getName();
                    if (file.isFile()) {
                        long size = file.length();
                        opSize += size;
                    }

//                    Thread.sleep(1);
                    file.delete();
                    opNums++;
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            nowDes = "删除失败：" + ex.getMessage();
            errorMsg = "删除失败：" + ex.getMessage();
        }
        endTime = System.currentTimeMillis();
        log.info("删除完成:" + Arrays.toString(fileAction.srcPaths));
    }
}

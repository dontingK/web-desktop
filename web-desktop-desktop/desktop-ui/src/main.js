import Vue from 'vue'
import App from './App.vue'
import 'ant-design-vue/dist/antd.css';
import antd from 'ant-design-vue'
import axios from 'axios'
import qs from 'qs'
import ContentMenu from "./meum/index"

console.log(process.env)

// let appAxios = axios.create()

axios.interceptors.request.use((config) => {
    console.log("----")
    // 如果是get请求，且params是数组类型如arr=[1,2]，则转换成arr=1&arr=2,不转换会显示为arr[]=1&arr[]=2
    config.paramsSerializer = function (params) {
        return qs.stringify(params, {arrayFormat: 'repeat'})
    }
    return config;
}, (error) => {
    return Promise.reject(error);
});


Vue.config.productionTip = false
Vue.use(antd)
Vue.use(ContentMenu)
Vue.prototype.$axios = axios;
Vue.prototype.$env = process.env.NODE_ENV;
new Vue({
    render: h => h(App),
}).$mount('#app')

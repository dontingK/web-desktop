package cn.donting.web.desktop.core.app;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * 根据request 获取AppId
 * @author donting
 * 2021-06-06 下午3:32
 */
public interface AppGetting {

    String REQUEST_APP_ID = "request_app_id";
    String REQUEST_ORIGIN_URL = "request_origin_url";

    /**
     * 根据 Request 获取 app Id
     *
     * @param request
     * @return
     */
    Long getAppId(ServletRequest request);

    /**
     * request 原始请求路径
     * 用于 springboot ErrorController 404 请求转发到 开发html，前后端分离
     * @param request
     * @return
     */
    default String getRequestOriginUrl(ServletRequest request) {
        return request.getAttribute(REQUEST_ORIGIN_URL).toString();
    }



}

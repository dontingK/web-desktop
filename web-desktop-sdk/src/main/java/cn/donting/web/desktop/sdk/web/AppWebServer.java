package cn.donting.web.desktop.sdk.web;

import org.springframework.boot.autoconfigure.AutoConfigurationImportSelector;
import org.springframework.boot.web.server.WebServer;
import org.springframework.boot.web.server.WebServerException;

/**
 * @author donting
 * 2021-05-30 下午12:14
 */

public class AppWebServer implements WebServer {
    @Override
    public void start() throws WebServerException {
        System.out.println("AppWebServer start");
    }

    @Override
    public void stop() throws WebServerException {
        System.out.println("AppWebServer stop");
    }

    @Override
    public int getPort() {
        return -1;
    }
}

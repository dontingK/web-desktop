package cn.donting.web.desktop.core.domain;

import lombok.Data;

/**
 * @author donting
 */
@Data
public class Wap {
    Long id;
    String[] args;
}

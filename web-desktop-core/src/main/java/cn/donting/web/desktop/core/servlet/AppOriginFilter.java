package cn.donting.web.desktop.core.servlet;

import cn.donting.web.desktop.core.app.AppGetting;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 用于请求原始路径保存。servlet 内部调转后出现404 ，使用原始请求url 去转发该请求，用于前后端分离时的
 * 开发需要
 * @see cn.donting.web.desktop.sdk.web.controller.DevErrorController
 * @author donting
 * 2021-06-14 上午10:53
 */
@WebFilter(filterName = "AppOriginFilter", urlPatterns = "/*")
@Slf4j
public class AppOriginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        //设置原始 请求路径
        servletRequest.setAttribute(AppGetting.REQUEST_ORIGIN_URL, httpServletRequest.getRequestURI());
        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }

}

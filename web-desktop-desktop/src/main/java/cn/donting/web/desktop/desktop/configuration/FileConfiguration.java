package cn.donting.web.desktop.desktop.configuration;


import cn.donting.file.FileTaskManger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;

@Configuration
public class FileConfiguration {
    @Bean
    public FileTaskManger fileTaskManger(){
        return new FileTaskManger();
    }
}

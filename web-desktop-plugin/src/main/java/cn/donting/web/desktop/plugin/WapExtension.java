package cn.donting.web.desktop.plugin;

import org.gradle.api.Project;
import org.gradle.api.provider.Property;

import java.net.URL;

public class WapExtension {
    private final Project project;

    private Property<String> mainClass;

    private Property<URL> mainJar;

    public WapExtension(Project project) {
        this.project = project;
        this.mainClass = project.getObjects().property(String.class);
        this.mainJar = project.getObjects().property(URL.class);
    }

    public Property<String> getMainClass() {
        return this.mainClass;
    }

    public Property<URL> getMainJar() {
        return this.mainJar;
    }

    @Override
    public String toString() {
        return "WapExtension{" +
                "project=" + project +
                ", mainClass=" + mainClass +
                '}';
    }
}

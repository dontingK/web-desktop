package cn.donting.web.desktop.top.user;


import cn.donting.web.desktop.top.domain.LoginUser;

import java.util.HashMap;

public class UserSpaceService {

    private static final HashMap<String, LoginUser> loginUsers = new HashMap<>();


    public static LoginUser getLoginUserName(String sessionId) {
        return loginUsers.get(sessionId);
    }

    public static void setLoginUserName(String sessionId, String name,Long id) {
        loginUsers.put(sessionId, new LoginUser(name,id));
    }

    public static void removeLoginUserName(String sessionId) {
        loginUsers.remove(sessionId);
    }

}

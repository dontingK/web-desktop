package cn.donting.desktop.run.loader;

/**
 * @author donting
 * 2021-05-29 下午2:33
 */
public  abstract class AbsRunLoader {
   public abstract void run(String[] args) throws Exception;
}

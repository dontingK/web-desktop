package cn.donting.web.desktop.core.app.interfaces;

import cn.donting.web.desktop.core.entity.User;

public interface UserCenter {
    User getLoginUser();

    User getLoginUser(String sessionId);
}

package cn.donting.web.desktop.top.user;

import cn.donting.web.desktop.top.domain.LoginUser;

import java.io.File;

/**
 * 登录的用户空间
 */
public interface LoginUserSpace {
//    String getUserName();
    LoginUser getLoginUser();
    File getDesktop();
    File getUserSpace();
}

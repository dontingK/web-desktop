package cn.donting.web.desktop.sdk.web.user;

import cn.donting.web.desktop.top.domain.LoginUser;
import cn.donting.web.desktop.top.user.LoginUserSpace;
import cn.donting.web.desktop.top.user.UserSpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.File;

@Service
public class UserSpace implements LoginUserSpace {

    private static final String SYS_PATH = System.getProperty("user.dir") + File.separator + "sys";
    private static final String USER_PATH = SYS_PATH + File.separator + "users";

    @Autowired
    HttpSession session;


    @Override
    public LoginUser getLoginUser() {
        return UserSpaceService.getLoginUserName(session.getId());
    }

    @Override
    public File getDesktop() {
        String userName = getLoginUser().getName();
        File userSpace = getUserSpace();
        File desktop=new File(userSpace.getPath()+File.separator+"desktop");
        return desktop;
    }

    @Override
    public File getUserSpace() {
        String userName = getLoginUser().getName();
        File file=new File(USER_PATH+File.separator+userName);
        return file;
    }
}

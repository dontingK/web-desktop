package cn.donting.web.desktop.file.resources.domain;

import lombok.Builder;
import lombok.Data;

/**
 * @author donting
 * 2021-06-06 下午4:06
 */
@Data
@Builder
public class ResponseEntity<T> {

    private int status;
    private String msg;
    private T data;


    public static ResponseEntity ok(Object data) {
        ResponseEntity<Object> build = ResponseEntity.builder().status(200).data(data).build();
        return build;
    }

    public static ResponseEntity ok(int status, Object data) {
        ResponseEntity<Object> build = ResponseEntity.builder().status(status).data(data).build();
        return build;
    }

    public static ResponseEntity error(int status, String msg) {
        ResponseEntity<Object> build = ResponseEntity.builder().status(status).msg(msg).build();
        return build;
    }

    public static ResponseEntity error(String msg) {
        ResponseEntity<Object> build = ResponseEntity.builder().status(500).msg(msg).build();
        return build;
    }

    public static ResponseEntity error(String msg, Object data) {
        ResponseEntity<Object> build = ResponseEntity.builder().status(500).msg(msg).data(data).build();
        return build;
    }


}

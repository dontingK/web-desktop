package cn.donting.web.desktop.core.loader;

import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.BootApp;
import cn.donting.web.desktop.core.entity.AppInfo;
import cn.donting.web.desktop.core.exception.LoadException;
import cn.donting.web.desktop.top.App;

import java.io.File;

public abstract class AppLoader {

    public abstract boolean isLoader(File file);

    public abstract Application loader(File file, AppInfo appInfo) throws Exception;

    protected final Application creatApp(String mainClass, AppURLClassLoader appURLClassLoader, AppInfo appInfo) throws ClassNotFoundException {
        Class<?> aClass = appURLClassLoader.loadClass(mainClass);
        App app = aClass.getAnnotation(App.class);
        if (app == null) {
            throw new LoadException(aClass.toString() + " not found app Annotation ");
        }
        appURLClassLoader.setApp(app);
        appURLClassLoader.setName(app.name());
        appURLClassLoader.setAppInfo(appInfo);
        Application application = new BootApp(appURLClassLoader, app, aClass);
        application.setAppInfo(appInfo);
        return application;
    }

}

package cn.donting.web.desktop.top;

import java.lang.annotation.Annotation;

/**
 * @author donting
 * 2021-05-30 下午3:27
 */
public interface AppContext extends AppDispatcher{
    String getProperties(String key);
    String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType);
    Class<?> getType(String name);
    <T> T getBean(Class<T> requiredType) throws Exception;
}

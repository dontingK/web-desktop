package cn.donting.web.desktop.core.app;

import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.dao.AppInfoRepository;
import cn.donting.web.desktop.core.dao.SysSettingRepository;
import cn.donting.web.desktop.core.entity.AppInfo;
import cn.donting.web.desktop.core.entity.SysSetting;
import cn.donting.web.desktop.core.exception.AppRuntimeException;
import cn.donting.web.desktop.top.AppType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 桌面管理者
 *
 * @author donting
 * 2021-06-13 下午2:04
 */
@Service
public class DesktopManager {

    /**
     * SysSetting 的 默认桌面AppId
     */
    public static final String DEFAULT_DESKTOP_ID = "DefaultDesktopId";
    /**
     * SysSetting 的 默认文件文件资源管理的AppId
     */
    public static final String DEFAULT_FILE_SOURCES_ID = "DefaultFileResources";

    private Application nowDesktop;

    @Autowired
    SysSettingRepository sysSettingRepository;
    @Autowired
    AppInfoRepository appInfoRepository;


    /**
     * 获取默认桌面的AppId
     *
     * @return null==无
     */
    public Long getWebDesktopAppId() {
        SysSetting byKey = sysSettingRepository.findByKey(DEFAULT_DESKTOP_ID);
        if (byKey == null) {
            return null;
        }
        return Long.parseLong(byKey.getValue());
    }

    /**
     * 文件文件资源管理的AppId
     *
     * @return null==无
     */
    public Long getFileResourcesAppId() {
        SysSetting byKey = sysSettingRepository.findByKey(DEFAULT_FILE_SOURCES_ID);
        if (byKey == null) {
            return null;
        }
        return Long.parseLong(byKey.getValue());
    }

    /**
     * 设置 默认桌面
     *
     * @throws AppRuntimeException
     */
    public void setWebDesktopId(String id) throws AppRuntimeException {
        AppInfo appInfo = appInfoRepository.findById(Long.parseLong(id)).get();
        if (appInfo == null) {
            throw new AppRuntimeException(appInfo.toString() + " app 不存在");
        }
        if (!appInfo.getType().equals(AppType.Desktop)) {
            throw new AppRuntimeException(appInfo.toString() + " 不是一个桌面应用");
        }
        SysSetting sysSetting = new SysSetting();
        sysSetting.setKey(DEFAULT_DESKTOP_ID);
        sysSetting.setValue(id);
        sysSettingRepository.save(sysSetting);
    }

    /**
     * 设置 文件资源管理
     *
     * @throws AppRuntimeException
     */
    public void setFileResourcesId(String id) throws AppRuntimeException {
        AppInfo appInfo = appInfoRepository.findById(Long.parseLong(id)).get();
        if (appInfo == null) {
            throw new AppRuntimeException(appInfo.toString() + " app 不存在");
        }
        if (!appInfo.getType().equals(AppType.FileResources)) {
            throw new AppRuntimeException(appInfo.toString() + " 不是一个资源管理器");
        }
        SysSetting sysSetting = new SysSetting();
        sysSetting.setKey(DEFAULT_FILE_SOURCES_ID);
        sysSetting.setValue(id);
        sysSettingRepository.save(sysSetting);
    }

    public Application getNowDesktop() {
        return nowDesktop;
    }

    public void setNowDesktop(Application application) {
        nowDesktop = application;
    }
}

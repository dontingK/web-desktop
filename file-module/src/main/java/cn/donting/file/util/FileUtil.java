package cn.donting.file.util;

import java.io.File;

public class FileUtil {

    /**
     * 同步 防止并发
     * @param fileName  fileName
     * @param prent 父级目录
     * @return
     */
    public static synchronized String fileName(String fileName, File prent){
        File file = new File(prent.getPath()+File.separator+fileName);
        int count=0;
        while (file.exists()){
            file=new File(prent.getPath()+File.separator+name(file.getName(),++count));
        }
        return file.getName();
    }

    public static synchronized String name(String name,int count){
        String extName = cn.hutool.core.io.FileUtil.extName(name);
        if(extName!=null&&!extName.equals("")){
            name= name.substring(0,name.lastIndexOf(extName)-1);
        }
        return name+"("+count+")"+extName;
    }

    public static void main(String[] args) {
//        String s = fileName(new File("D:\\新建文件夹\\web-desktop\\sys\\a"), new File("D:\\新建文件夹\\web-desktop\\sys"));
//        System.out.println(s);
    }

}

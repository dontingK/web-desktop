package cn.donting.web.desktop.core.loader;

import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.entity.AppInfo;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

public class AppDevLoader extends AppLoader {
    @Override
    public boolean isLoader(File file) {
        if(file.getName().endsWith("classpath")){
            return true;
        }
        return false;
    }

    @Override
    public Application loader(File file, AppInfo appInfo) throws IOException, ClassNotFoundException {
        FileInputStream inputStream = new FileInputStream(file);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String mainClass = bufferedReader.readLine();
        String libPath;
        ArrayList<URL> urls=new ArrayList<>();
        while((libPath = bufferedReader.readLine()) != null)
        {
           urls.add(new File(libPath).toURI().toURL());
        }
        AppURLClassLoader appURLClassLoader=new AppURLClassLoader(urls.toArray(new URL[0]),AppDevLoader.class.getClassLoader().getParent());
        appURLClassLoader.setAppInfo(appInfo);
        Application application = creatApp(mainClass, appURLClassLoader,appInfo);
        return application;
    }
}

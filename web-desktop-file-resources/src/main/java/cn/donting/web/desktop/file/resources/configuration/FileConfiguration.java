package cn.donting.web.desktop.file.resources.configuration;


import cn.donting.file.FileTaskManger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileConfiguration {
    @Bean
    public FileTaskManger fileTaskManger(){
        return new FileTaskManger();
    }

}

package cn.donting.web.desktop.sdk.web.configuration;

import cn.donting.web.desktop.sdk.web.controller.DevErrorController;
import cn.donting.web.desktop.sdk.web.user.UserSpace;
import cn.donting.web.desktop.top.interfaces.DesktopResources;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class AutoConfiguration {
    public AutoConfiguration() {
//        Properties properties = System.getProperties();
    }


    @Bean
    @ConditionalOnProperty(prefix="webapp.dev",name = "host")
    public DevErrorController errorController() {
        return new DevErrorController();
    }

    @Bean
    public UserSpace userSpace() {
        return new UserSpace();
    }

    /**
     * 注入默认的 Desktop
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(DesktopResources.class)
    public DesktopResources desktop() {
        return () -> "/sys/appSdk.js";
    }
}

package cn.donting.web.desktop.desktop;

import cn.donting.web.desktop.top.App;
import cn.donting.web.desktop.top.AppType;
import cn.donting.web.desktop.top.interfaces.DesktopResources;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;

/**
 * @author donting
 * 2021-06-13 下午5:20
 */
@SpringBootApplication
@App(name = "桌面",version = "0.1",numberVersion = 1,index = "/desktop.html",type = AppType.Desktop,icon="app.png")

public class DesktopApplication implements DesktopResources {
    public static void main(String[] args) {
        SpringApplication.run(DesktopApplication.class,args);
    }

    @Override
    public String webAppSdkUrl() {
        return "/appSdk.js";
    }
}

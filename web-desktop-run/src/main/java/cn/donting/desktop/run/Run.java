package cn.donting.desktop.run;

import cn.donting.desktop.run.loader.AbsRunLoader;
import cn.donting.desktop.run.loader.SysDevRunLoader;
import org.springframework.context.ApplicationContext;

import java.util.Map;
import java.util.Properties;

/**
 * @author donting
 * 2021-05-29 上午11:58
 */
public class Run {
    public static void main(String[] args) throws Exception {
//        Properties properties = System.getProperties();
//        properties.setProperty("user.dir","自定义");
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        AbsRunLoader runLoader=new SysDevRunLoader();
        runLoader.run(args);
//        Map<String, String> getenv = System.getenv();
//        System.out.println("111");
    }
}

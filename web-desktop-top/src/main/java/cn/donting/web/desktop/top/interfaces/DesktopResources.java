package cn.donting.web.desktop.top.interfaces;

/**
 * 桌面 appSdk url
 */
public interface DesktopResources {
    String webAppSdkUrl();
}

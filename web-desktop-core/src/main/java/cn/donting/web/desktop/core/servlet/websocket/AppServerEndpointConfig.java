package cn.donting.web.desktop.core.servlet.websocket;

import cn.donting.web.desktop.core.Application;
import org.eclipse.jetty.websocket.common.scopes.WebSocketContainerScope;
import org.eclipse.jetty.websocket.jsr356.server.AnnotatedServerEndpointConfig;

import javax.websocket.DeploymentException;
import javax.websocket.server.ServerEndpoint;

/**
 * @author donting
 * 2021-06-28 下午9:15
 */
public class AppServerEndpointConfig extends AnnotatedServerEndpointConfig {

    private final Application application;

    public AppServerEndpointConfig(Application application, WebSocketContainerScope containerScope, Class<?> endpointClass, ServerEndpoint anno) throws DeploymentException {
        super(containerScope, endpointClass, anno);
        this.application=application;
    }

//    public AppServerEndpointConfig(WebSocketContainerScope containerScope, Class<?> endpointClass, ServerEndpoint anno, ServerEndpointConfig baseConfig) throws DeploymentException {
//        super(containerScope, endpointClass, anno, baseConfig);
//    }


    @Override
    public Configurator getConfigurator() {
        return new AppEndpointConfig(super.getConfigurator());
    }

    @Override
    public String getPath() {
        return "/"+application.getId()+super.getPath();
    }
}

package cn.donting.web.desktop.desktop.vo;


import cn.hutool.core.io.FileUtil;
import lombok.Data;

import java.io.File;

@Data
public class FileVo {

    String filePath;
    String fileName;
    String extName;
    Boolean file;

    public FileVo(String filePath, String fileName, String extName, Boolean file) {
        this.filePath = filePath;
        this.fileName = fileName;
        this.extName = extName;
        this.file = file;
    }

    public static FileVo build(File file){
      return   new FileVo(file.getPath(),file.getName(), FileUtil.extName(file),file.isFile());
    }
}

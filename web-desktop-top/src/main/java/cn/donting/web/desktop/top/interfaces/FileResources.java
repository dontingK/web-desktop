package cn.donting.web.desktop.top.interfaces;

/**
 * 文件资源
 */
public interface FileResources {
    /**
     * 选择文件的FileUrl
     * @return
     */
    String selectFileUrl();
}

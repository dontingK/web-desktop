package cn.donting.web.desktop.top.utils;

public class UniqueIdUtil {
    private static Long uniqueId=0L;

    public static synchronized Long getUniqueId(){
        return ++uniqueId;
    }
}

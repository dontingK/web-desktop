package cn.donting.desktop.run.loader;

import java.lang.reflect.Method;

/**
 * @author donting
 * 2021-05-29 下午2:34
 */
public class SysDevRunLoader extends AbsRunLoader{
    @Override
    public void run(String[] args) throws Exception {
        SysDevClassLoader sysDevClassLoader= (SysDevClassLoader) SysDevClassLoader.getInstance();
        Class<?> mainClass = sysDevClassLoader.getMainClass();
        Thread.currentThread().setContextClassLoader(sysDevClassLoader);
        Method main = mainClass.getMethod("main", String[].class);
        main.invoke(null,(Object) args);

    }
}

package cn.donting.web.desktop.core.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author donting
 * 2021-06-12 下午2:32
 */
@Entity
@Data
public class OpenFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * 扩展名
     * null == 文件夹
     * "" == 未知文件
     */
    @Column
    private String extName;

    /**
     * 打开的appId
     */
    @Column
    private Long appId;

    /**
     *
     */
    @Column
    private String icon;
    /**
     *
     */
    @Column
    private String fileName;

    /**
     * 打开的对应文件的 url
     */
    @Column
    private String openFileUrl;
    /**
     * 是否默认
     */
    @Column
    private boolean defaultValue;


}

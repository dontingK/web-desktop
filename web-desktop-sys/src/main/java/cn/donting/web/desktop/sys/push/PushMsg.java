package cn.donting.web.desktop.sys.push;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
public class PushMsg<T,V> {
    private PushMsgEnum pushMsgEnum;
    private T msg;
    private Long id;
    /**
     * 超时 5s
     */
    @Setter
    private long timeout = 5000;

    private static Long msgId = 1L;

    @Setter
    private V res;

    private static synchronized Long creatId() {
        return ++msgId;
    }

    public PushMsg(PushMsgEnum pushMsgEnum, T msg) {
        this.pushMsgEnum = pushMsgEnum;
        this.msg = msg;
        id = creatId();
    }
}

package cn.donting.file.vo;

import cn.donting.file.FileAction;
import cn.donting.file.FileTask;
import cn.hutool.core.io.unit.DataSize;
import lombok.Getter;

/**
 * @author donting
 * 2021-07-11 下午5:32
 */
@Getter
public class FileTaskVo {

    private FileAction fileAction;
    private int fileNums = 0;
    private long countSize = 0;
    private long opSize = 0;
    private long opNums = 0;
    private String nowDes = "";
    private long beginTime;
    private long endTime;
    private String errorMsg;
    private String taskId;

    private boolean cancel = false;


    public FileTaskVo(FileTask fileTask) {
        this.fileAction=fileTask.getFileAction();
        this.fileNums=fileTask.getFileNums();
        this.countSize=fileTask.getCountSize();
        this.opSize=fileTask.getOpSize();
        this.opNums=fileTask.getOpNums();
        this.nowDes=fileTask.getNowDes();
        this.beginTime=fileTask.getBeginTime();
        this.endTime=fileTask.getEndTime();
        this.errorMsg=fileTask.getErrorMsg();
        this.taskId=fileTask.getTaskId();
    }
}

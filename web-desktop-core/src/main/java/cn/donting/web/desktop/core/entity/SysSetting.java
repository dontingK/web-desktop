package cn.donting.web.desktop.core.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author donting
 * 2021-06-13 下午1:52
 */
@Entity
@Data
public class SysSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String key;
    @Column
    private String value;


}

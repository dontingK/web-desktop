package cn.donting.web.desktop.top.domain;

import lombok.Data;

import java.util.Objects;

@Data
public class LoginUser {
    private String name;
    private Long userId;

    public LoginUser(String name, Long userId) {
        this.name = name;
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginUser loginUser = (LoginUser) o;
        return Objects.equals(name, loginUser.name) && Objects.equals(userId, loginUser.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, userId);
    }
}

import Vue from 'vue'
import App from './App.vue'
import 'ant-design-vue/dist/antd.css';
import antd from 'ant-design-vue'
import axios from 'axios'

console.log(process.env)

Vue.config.productionTip = false
Vue.use(antd)
Vue.prototype.$axios=axios;
Vue.prototype.$env=process.env.NODE_ENV;
new Vue({
    render: h => h(App),
}).$mount('#app')

package cn.donting.web.desktop.top;

import cn.donting.web.desktop.top.utils.UniqueIdUtil;

public interface AutoId {
    default Long getId() {
        return UniqueIdUtil.getUniqueId();
    }
}

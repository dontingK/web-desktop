package cn.donting.web.desktop.core.dao;

import cn.donting.web.desktop.core.entity.OpenFile;
import cn.donting.web.desktop.core.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author donting
 * 2021-06-06 下午8:50
 */
@Repository
public interface OpenFileRepository extends JpaRepository<OpenFile,Long> {
    List<OpenFile> findByAppId(Long appId);
    int deleteByAppId(Long appId);
    OpenFile findByExtNameAndDefaultValue(String extName,boolean isDefault);
    OpenFile findByExtNameIsNullAndDefaultValue(boolean isDefault);
    OpenFile findFirstByExtName(String extName);
    OpenFile findFirstByExtNameIsNull();

}

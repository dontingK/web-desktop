package cn.donting.web.desktop.core.dao;

import cn.donting.web.desktop.core.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author donting
 * 2021-06-06 下午8:50
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findByNameAndPassword(String name,String password);
    User findByName(String name);
}

package cn.donting.web.desktop.sdk.web;

import org.apache.catalina.core.StandardHost;
import org.apache.tomcat.util.http.parser.Host;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.server.AbstractServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

/**
 * @author donting
 * 2021-05-30 下午12:11
 */
public class AppServletWebServerFactory extends TomcatServletWebServerFactory {
    @Override
    public WebServer getWebServer(ServletContextInitializer... initializers) {
        prepareContext(new StandardHost(), initializers);
        return new AppWebServer();
    }
}

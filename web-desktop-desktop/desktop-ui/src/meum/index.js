import ContentMenu from "./ContentMenu"
// import Vue from 'vue'
const ContentMenuExport={
    // install 是默认的方法。当外界在 use 这个组件的时候，就会调用本身的 install 方法，同时传一个 Vue 这个类的参数。
    install:function(Vue){
        const contentMenu = Vue.extend(ContentMenu)
        const div = document.createElement('div')
        document.body.appendChild(div)
        const vm = new contentMenu({
        }).$mount(div)
        vm.innerShowPopup = true
        Vue.prototype.$contentMenu = vm
    }
}

export default ContentMenuExport
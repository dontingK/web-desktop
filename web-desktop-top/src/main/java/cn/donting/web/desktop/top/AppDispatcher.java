package cn.donting.web.desktop.top;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * @author donting
 * 2021-05-30 下午3:04
 */
public interface AppDispatcher {
    /**
     * doService
     * @param httpRequest
     * @param httpResponse
     */
    void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException;
}

package cn.donting.web.desktop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author donting
 * 2021-06-20 下午3:42
 */
public class LoginUiCopy {

    public static void main(String[] args) throws IOException {

        String path = System.getProperty("user.dir");

        File file = new File(path + File.separator + "dist");

        List<String> dir = dir(file);

        file = new File(path+File.separator+"loginFiles.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fileOutputStream=new FileOutputStream(file);
        String txt="";
        for (String s : dir) {
            txt+=s+"\n";
        }
        fileOutputStream.write(txt.getBytes());
        fileOutputStream.close();

    }

    public static List<String> dir(File file) {
        ArrayList<String> strings = new ArrayList<>();
        for (File listFile : file.listFiles()) {
            if (listFile.isDirectory()) {
                List<String> dir = dir(file);
                strings.addAll(dir);
            } else {
                strings.add(listFile.getPath());
            }
        }
        return strings;
    }
}

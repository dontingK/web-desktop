let autoWindowId = 1;
let autoWindowTopZIndex = 200;
const fileClipboard = {
    files: [],
    type: null,
}
//windowApi 集合
const windowApiManager = {
    "windowId": "windowApi",
}

window.getWindowApi=(windowId)=>{
    return windowApiManager[windowId]
}
//windows 集合
let windows = null;
let contentMenu=null;
let refreshDesktop=null;
class WindowApi {
    #window;

    constructor(window) {
        this.#window = window;
    }

    set title(title) {
        this.#window.title = title;
    }

    set icon(icon) {
        this.#window.icon = icon;
    }

    set show(show) {
        this.#window.show = show;
    }

    set size(size) {
        this.#window.size = size;
    }
    get param(){
        return this.#window.param
    }
    refreshDesktop(){
        refreshDesktop();
    }
    getFileClipboard() {
        return {
            files: fileClipboard.files,
            type: fileClipboard.type,
        }
    }

    setFileClipboard(fc) {
        let temp = fileClipboard.files;
        if (!!fc && !!fc.files && Array.isArray(fc.files)) {
            fileClipboard.files = fc.files;
        } else {
            console.error("setFileClipboard.files", fc)
            throw new Error("files in not Array")
        }
        if (!!fc.type) {
            fileClipboard.type = fc.type;
        } else {
            fileClipboard.files = temp;
            console.error("setFileClipboard.type", fc)
            throw new Error("type in not found")
        }
        return true;
    }

    publishEvent(event, data) {
        this.#window.publishEvent(event, data)
    }

    openFile(filePath) {
        this.#window.publishEvent("openFile", filePath)
    }

    openWindow(url, title, icon, param) {
        this.#window.publishEvent("openWindow", {url, title, icon, param})
    }

    openMenu(e, menus) {
        this.#window.publishEvent("openMenu", {e, menus})
    }

    registerEvent(event, callback) {
        this.#window.registerEvent(event, callback)
    }

    close() {
        this.#window.publishEvent("windowClose")
    }
}

class AppWindow {
    #windowId = ++autoWindowId;
    #zIndex = ++autoWindowTopZIndex;
    #url
    #title
    #icon
    #param
    #events = {}
    #appId = null
    #show = true
    #changeSize = true
    #windowApi
    #refreshDesktop=refreshDesktop
    #size = {
        width: 200,
        height: 200,
        minWidth: 100,
        minHeight: 100,
        maxWidth: 500,
        maxHeight: 500,
    }

    constructor(option) {
        this.#_constructor(option)
    }

    #_constructor(option) {
        if (!!option.url) {
            this.#url = option.url;
        } else {
            throw  new Error("url 不能为空")
        }
        if (!!option.title) {
            this.#title = option.title;
        }
        if (!!option.icon) {
            this.#icon = option.icon;
        }
        if (!!option.param) {
            this.#param = option.param;
        }
        if (!!option.size) {
            this.size=option.size
        }
        if ( option.changeSize==false || option.changeSize==true) {
            this.#changeSize = option.changeSize
        }
        this.registerEvent("windowFocus", () => {
            this.#zIndex = ++autoWindowTopZIndex;
        })
        this.#windowApi = new WindowApi(this);
        windowApiManager[this.#windowId] = this.#windowApi;
        this.registerEvent("windowClose", () => {
            delete windowApiManager[this.#windowId]
            for (let i = 0; i <windows.length ; i++) {
                let window=windows[i]
                if (window.windowId === this.#windowId){
                    windows.splice(i,1)
                    return;
                }
            }
        })
        windows.push(this);
    }

    get windowId() {
        return this.#windowId;
    }

    get url() {
        return this.#url;
    }

    get title() {
        return this.#title;
    }

    get changeSize() {
        return this.#changeSize;
    }

    set changeSize(changeSize) {
        this.#changeSize = changeSize;
    }

    set title(title) {
        this.#title = title
    }

    get icon() {
        return this.#icon;
    }

    set icon(icon) {
        return this.#icon = icon;
    }

    get param() {
        return this.#param;
    }

    get zIndex() {
        return this.#zIndex;
    }

    get show() {
        return this.#show;
    }

    set show(show) {
        return this.#show = show;
    }

    get size() {
        return {
            width: this.#size.width,
            height: this.#size.height,
            minWidth: this.#size.minWidth,
            minHeight: this.#size.minHeight,
            maxWidth: this.#size.maxWidth,
            maxHeight: this.#size.maxHeight,
        };
    }

    set size(size) {
        // this.#setSize(size)
        if (!!size.width) {
            this.#size.width = size.width;
        }
        if (!!size.height) {
            this.#size.height = size.height;
        }
        if (!!size.minWidth) {
            this.#size.minWidth = size.minWidth;
        }
        if (!!size.minHeight) {
            this.#size.minHeight = size.minHeight;
        }
        if (!!size.maxWidth) {
            this.#size.maxWidth = size.maxWidth;
        }
        if (!!size.maxHeight) {
            this.#size.maxHeight = size.maxHeight;
        }
    }

    set appId(appId) {
        if (this.#appId == null) {
            this.#appId = appId;
        } else {
            console.warn("重复设置appId")
        }
    }

    get appId() {
        return this.#appId;
    }


    publishEvent(event, data) {
        if (this.#events[event]) {
            this.#events[event].forEach(callback => {
                if (typeof callback === "function") {
                    callback(data);
                } else {
                    console.log(callback, "is not function：event[" + event + "]")
                }
            })
        }
    }

    registerEvent(event, callback) {
        if (!this.#events[event]) {
            this.#events[event] = [];
        }
        this.#events[event].push(callback)
    }
}


export default {
    setFileClipboardFiles: (files) => {
        fileClipboard.files = files;
    },
    getFileClipboardFiles: () => {
        return fileClipboard.files
    },
    getFileClipboardType: () => {
        return fileClipboard.type
    },
    setFileClipboardType: (type) => {
        fileClipboard.type = type;
    },
    setWindows:(wins)=>{
        if (windows == null) {
            windows=wins;
        }else{
            console.log("windows not null")
        }
    },
    setContentMenu:(cm)=>{
        contentMenu=cm;
    },
    setRefreshDesktop:(rd)=>{
        refreshDesktop=rd;
    },
    creatWindow(op) {
        return new AppWindow(op)
    }
}

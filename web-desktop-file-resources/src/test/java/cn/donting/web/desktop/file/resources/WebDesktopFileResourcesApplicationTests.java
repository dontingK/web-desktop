package cn.donting.web.desktop.file.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import oshi.SystemInfo;
import oshi.software.os.FileSystem;
import oshi.software.os.OSDesktopWindow;
import oshi.software.os.OSFileStore;

import java.util.List;
import java.util.Properties;

//@SpringBootTest
class WebDesktopFileResourcesApplicationTests {

    @Test
    void contextLoads() throws JsonProcessingException {
        Properties properties = System.getProperties();
        SystemInfo si =  new  SystemInfo();
        FileSystem fileSystem = si.getOperatingSystem().getFileSystem();
        List<OSFileStore> fileStores = fileSystem.getFileStores();
        List<OSFileStore> fileStores1 = fileSystem.getFileStores(true);
        ObjectMapper objectMapper=new ObjectMapper();
        String s = objectMapper.writeValueAsString(fileStores);
        System.out.println();
    }

}

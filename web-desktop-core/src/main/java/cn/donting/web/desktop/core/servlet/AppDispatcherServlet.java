package cn.donting.web.desktop.core.servlet;


import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.app.AppGetting;
import cn.donting.web.desktop.core.app.AppManager;
import cn.donting.web.desktop.core.app.DesktopManager;
import cn.donting.web.desktop.core.entity.AppInfo;
import cn.donting.web.desktop.top.AppType;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static cn.donting.web.desktop.core.app.AppGetting.REQUEST_APP_ID;

/**
 * 用于分发http请求
 * 该类  中不能注入 httpServlet 相关，此时httpServlet 还未绑定到线程中
 * @author donting
 * @see org.springframework.web.servlet.FrameworkServlet#processRequest
 * 2021-05-30 下午2:41
 */
public class AppDispatcherServlet extends DispatcherServlet {
    @Autowired
    private AppManager appManager;

    @Autowired
    AppGetting appGetting;

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    DesktopManager desktopManager;

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        Long requestAppId = appGetting.getAppId(request);
        if(requestAppId.equals(Application.sysAppId)){
            if(request.getRequestURI().equals("/")){
                //进入默认桌面
                Long webDesktopAppId = desktopManager.getWebDesktopAppId();
                if (webDesktopAppId == null) {
                    request.setAttribute(REQUEST_APP_ID,Application.sysAppId);
                    String path = StrUtil.format("/sys/app/error?msg={}", "没有默认桌面");
                    req.getRequestDispatcher(path).forward(req, res);
                    return;
                }
                AppInfo appInfo = appManager.getAppInfo(webDesktopAppId);
                String index = appInfo.getIndex();
                response.sendRedirect(index+"?appId="+appInfo.getAppId());
            }else{
                super.service(req, res);
            }
            return;
        }

        Application app = appManager.getApp(requestAppId);
        if (app == null) {
            try {
               app= appManager.startApp(requestAppId);
            } catch (Exception e) {
                logger.error(e.getMessage(),e);
                String msg=StrUtil.format("app[{}]启动失败：{}",requestAppId,e.getMessage());
//                request.setAttribute(REQUEST_APP_ID,Application.sysAppId);
                String path = StrUtil.format("/sys/app/error?msg={}", msg);
                req.getRequestDispatcher(path).forward(req, res);
                return;
            }
        }
        if(app.getAppInfo().getType().equals(AppType.Desktop)){
            desktopManager.setNowDesktop(app);
        }
        app.getAppContext().doService(req, res);
    }




}

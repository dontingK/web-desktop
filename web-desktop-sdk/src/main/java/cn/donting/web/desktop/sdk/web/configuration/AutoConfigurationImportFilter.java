package cn.donting.web.desktop.sdk.web.configuration;

import org.springframework.boot.autoconfigure.AutoConfigurationMetadata;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;

/**
 * AutoConfiguration 过滤器
 */
public class AutoConfigurationImportFilter implements org.springframework.boot.autoconfigure.AutoConfigurationImportFilter {

    /**
     * 去除 ServletWebServerFactoryAutoConfiguration 的自动配置
     * @see  cn.donting.web.desktop.sdk.web.AppServletWebServerFactoryAutoConfiguration
     */
    public static final String ServletWebServerFactoryAutoConfiguration = ServletWebServerFactoryAutoConfiguration.class.getName();

    @Override
    public boolean[] match(String[] autoConfigurationClasses, AutoConfigurationMetadata autoConfigurationMetadata) {
        boolean[] booleans = new boolean[autoConfigurationClasses.length];
        for (int i = 0; i < autoConfigurationClasses.length; i++) {
            if (!autoConfigurationClasses.equals(ServletWebServerFactoryAutoConfiguration)) {
                booleans[i] = true;
            }
        }
        return booleans;
    }
}

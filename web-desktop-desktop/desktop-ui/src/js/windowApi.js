// const windowApiManager = {}
// const fileClipboard = {
//     files: [],
//     type: "copy"
// }
//
// let windowManager = null;
//
// let windowTopZIndex = 201
// let windowId = 1
// let contentMenu = null;
//
// self.getWindowApi = function (windowId) {
//     return windowApiManager[windowId];
// }
//
// const fileClipboardObject = {
//     getFileClipboardFiles: () => {
//         return fileClipboard.files;
//     },
//     getFileClipboardType: () => {
//         return fileClipboard.type;
//     },
//     setFileClipboardFiles: (files) => {
//         fileClipboard.files = files;
//     },
//     setFileClipboardType: (type) => {
//         fileClipboard.type = type;
//     }
// }
//
// function creatWindowApi(window) {
//     const windowApi = {
//         setTitle: (title) => {
//             window.title = title;
//         },
//         setIcon: (icon) => {
//             if(icon.indexOf("\?")> -1){
//                 window.icon = icon+"&appId="+windowApi.appId;
//             }else{
//                 window.icon = icon+"?appId="+windowApi.appId;
//             }
//         },
//         openFile: (filePath) => {
//             openFile(filePath)
//         },
//         openWindow: (url, param) => {
//             openWindow(url, param)
//         },
//         openMenu(e, menu) {
//             contentMenu.open(e, menu)
//         },
//         close() {
//             window.publishEvent("windowClose");
//         },
//         setVisible(show) {
//             window.show = show;
//
//         },
//         windowParam: window.param,
//         registerEvent: (event, callback) => {
//             if (!window.events[event]) {
//                 window.events[event] = [];
//             }
//             window.events[event].push(callback);
//         },
//         publishEvent: (event, data) => {
//             if (window.events[event]) {
//                 for (let i = 0; i < window.events[event].length; i++) {
//                     let callback = window.events[event][i];
//                     callback(data);
//                 }
//             } else {
//                 console.warn("Event:" + event + " 未注册")
//             }
//         },
//         fileClipboard: fileClipboardObject,
//         setWindowSize(width, height) {
//             window.size.width = width;
//             window.size.height = height;
//         },
//         getWindowId() {
//             return window.windowId;
//         },
//         //webAppSdk 回 设置
//         appId:"",
//     }
//     windowApiManager[window.windowId] = windowApi;
//     window.publishEvent = windowApi.publishEvent;
//     windowApi.registerEvent("windowFocus", function () {
//         window.zIndex = ++windowTopZIndex;
//     })
//     windowApi.registerEvent("windowClose", function () {
//         destroyedWidow(window);
//     })
// }
//
// function destroyedWidow(window) {
//     console.log("closeWindow", window)
//     for (let i = 0; i < windowManager.length; i++) {
//         if (window.windowId === windowManager[i].windowId) {
//             windowManager.splice(i, 1)
//             break
//         }
//     }
//     delete windowApiManager[window.windowId]
// }
//
// function openWindow(url, title, icon, param) {
//     // debugger
//     let window = {
//         windowId: ++windowId,
//         src: url,
//         title: title,
//         icon: icon,
//         zIndex: ++windowTopZIndex,
//         param: param,
//         events: {},
//         publishEvent: null,
//         show: true,
//         size: {
//             width: 200,
//             height: 200,
//         },
//     };
//     creatWindowApi(window);
//     console.log("creatWindowApi",window)
//     windowManager.push(window)
//
// }
//
// function openFile(filePath) {
//     openWindow("/sys/file/open?filePath=" + filePath,
//         filePath,
//         '/sys/file/icon?filePath=' + filePath,
//         null)
// }
//
// function setWindowManager(windows) {
//     if (windowManager == null) {
//         windowManager = windows;
//         console.log("windowManager",windowManager)
//     } else {
//         console.warn("windowManager is not null")
//     }
// }
//
// function setContentMenu(contentMenu1) {
//     if (contentMenu == null) {
//         contentMenu = contentMenu1;
//     } else {
//         console.warn("contentMenu is not null")
//     }
// }
//
// export default {
//     openWindow: openWindow,
//     openFile: openFile,
//     setWindowManager: setWindowManager,
//     setContentMenu: setContentMenu,
//     fileClipboard: fileClipboardObject
// }
let autoWindowId = 1;
let autoWindowTopZIndex = 200;
let appWindows = [];
let contentMenu = function () {
};
let refreshDesktop = function () {
};
const fileClipboard = new FileClipboard();

class FileClipboard {
    #files = []
    #type = null
    set files(files) {
        if (Array.isArray(files)) {
            this.#files = files;
        } else {
            console.error("setFileClipboard.files", fc)
            throw new Error("files in not Array")
        }
        return true;
    }
    set type(type) {
        this.#type = type;
    }
    get files() {
        return this.#files
    }
    get type() {
        return this.#type
    }
}

window.getAppWindow = (windowId) => {
    return appWindows[windowId]
}

export class AppWindow {
    #windowId = ++autoWindowId;
    #zIndex = ++autoWindowTopZIndex;
    #url=""
    #title=""
    #icon=""
    #param=""
    #events = {}
    #appId = null
    #show = true
    #changeSize = true
    #size = {
        width: 200,
        height: 200,
        minWidth: 100,
        minHeight: 100,
        maxWidth: 500,
        maxHeight: 500,
    }

    constructor(option) {
        this.#_constructor(option)
    }

    #_constructor(option) {
        if (!!option.url) {
            this.#url = option.url;
        } else {
            throw  new Error("url 不能为空")
        }
        if (!!option.title) {
            this.#title = option.title;
        }
        if (!!option.icon) {
            this.#icon = option.icon;
        }
        if (!!option.param) {
            this.#param = option.param;
        }
        if (!!option.size) {
            this.size = option.size
        }
        if (option.changeSize == false || option.changeSize == true) {
            this.#changeSize = option.changeSize
        }
        this.registerEvent("windowFocus", () => {
            this.#zIndex = ++autoWindowTopZIndex;
        })
        this.registerEvent("windowClose", () => {
            for (let i = 0; i < appWindows.length; i++) {
                let window = appWindows[i]
                if (window.windowId === this.#windowId) {
                    appWindows.splice(i, 1)
                    return;
                }
            }
        })
        appWindows.push(this)
    }

    get windowId() {
        return this.#windowId;
    }

    get url() {
        return this.#url;
    }

    get title() {
        return this.#title;
    }

    get changeSize() {
        return this.#changeSize;
    }

    set changeSize(changeSize) {
        this.#changeSize = changeSize;
    }

    set title(title) {
        this.#title = title
    }

    get icon() {
        return this.#icon;
    }

    set icon(icon) {
        return this.#icon = icon;
    }

    get param() {
        return this.#param;
    }

    get zIndex() {
        return this.#zIndex;
    }

    get show() {
        return this.#show;
    }

    set show(show) {
        return this.#show = show;
    }

    get size() {
        return {
            width: this.#size.width,
            height: this.#size.height,
            minWidth: this.#size.minWidth,
            minHeight: this.#size.minHeight,
            maxWidth: this.#size.maxWidth,
            maxHeight: this.#size.maxHeight,
        };
    }

    set size(size) {
        // this.#setSize(size)
        if (!!size.width) {
            this.#size.width = size.width;
        }
        if (!!size.height) {
            this.#size.height = size.height;
        }
        if (!!size.minWidth) {
            this.#size.minWidth = size.minWidth;
        }
        if (!!size.minHeight) {
            this.#size.minHeight = size.minHeight;
        }
        if (!!size.maxWidth) {
            this.#size.maxWidth = size.maxWidth;
        }
        if (!!size.maxHeight) {
            this.#size.maxHeight = size.maxHeight;
        }
    }

    set appId(appId) {
        if (this.#appId == null) {
            this.#appId = appId;
        } else {
            console.warn("重复设置appId")
        }
    }

    get appId() {
        return this.#appId;
    }

    get fileClipboard(){
        return fileClipboard;
    }

    publishEvent(event, data) {
        if (this.#events[event]) {
            this.#events[event].forEach(callback => {
                if (typeof callback === "function") {
                    callback(data);
                } else {
                    console.log(callback, "is not function：event[" + event + "]")
                }
            })
        }
    }

    registerEvent(event, callback) {
        if (!this.#events[event]) {
            this.#events[event] = [];
        }
        this.#events[event].push(callback)
    }

    close() {
        this.publishEvent("windowClose")
    }

    openFile(filePath, windowOp = {}) {
        windowOp.url = "/sys/file/openFile?filePath=" + filePath;
        return new AppWindow(windowOp)
    }

    openWindow(windowOp) {
        return new AppWindow(windowOp)
    }

    openContentMenu(e, menus) {
        contentMenu(e, menus)
    }

    refreshDesktop() {
        refreshDesktop();
    }

}

export function setContentMenu(cm) {
    contentMenu = cm;
}
export function setRefreshDesktop(rd) {
    refreshDesktop = rd;
}
export function setAppWindows(aw) {
    appWindows = aw
}
export function openWindow(op){
    return new AppWindow(op)
}
export function openFile(filePath,windowOp={}){
    windowOp.url = "/sys/file/openFile?filePath=" + filePath;
    return new AppWindow(windowOp)
}
export let fileClipboard=fileClipboard


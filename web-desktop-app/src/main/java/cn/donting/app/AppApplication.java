package cn.donting.app;

import cn.donting.web.desktop.top.App;
import cn.donting.web.desktop.top.file.FileOpen;
import cn.donting.web.desktop.top.file.FileRegister;
import org.apache.catalina.servlets.DefaultServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

//ServletWebServerFactoryAutoConfiguration.class
@SpringBootApplication()
@App(name = "testApp",version = "0.1",numberVersion = 1,index = "/index.html",icon = "app.png")
public class AppApplication  implements FileRegister {

    static {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        System.out.println(contextClassLoader);
    }


    public static void main(String[] args) throws InterruptedException, NoSuchMethodException {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        ConfigurableApplicationContext run = SpringApplication.run(AppApplication.class, args);
        DispatcherServlet bean = run.getBean(DispatcherServlet.class);
    }


//    @Override
//    public FileOpen[] register() {
////        return new FileOpen[]{new FileOpen("png","png.txt","/open")};
//    }
}

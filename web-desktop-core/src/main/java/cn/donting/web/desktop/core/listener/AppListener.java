package cn.donting.web.desktop.core.listener;

import cn.donting.web.desktop.core.Application;
import cn.donting.web.desktop.core.entity.AppInfo;

import java.net.URISyntaxException;

public interface AppListener {
    default void install(AppInfo appInfo) {
    }
    default void uninstall(AppInfo appInfo) {
    }
    default void init() throws Exception {
    }

    /**
     * 启动之前
     * @param appInfo
     */
    default void beforeStart(AppInfo appInfo){}

    /**
     * 启动之后
     * @param application
     */
    default void afterStart(Application application){}

    /**
     * 停止之前
     * @param application
     */
    default void beforeStop(Application application){};

    /**
     * 停止之后
     * @param application
     */
    default void afterStop(Application application){};

    default void afterUpdate(AppInfo application){};
}

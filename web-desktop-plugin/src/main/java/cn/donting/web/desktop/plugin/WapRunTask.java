package cn.donting.web.desktop.plugin;

import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.JavaExec;
import org.springframework.boot.loader.tools.JarWriter;
import org.springframework.boot.loader.tools.Libraries;
import org.springframework.boot.loader.tools.LibraryCallback;
import org.springframework.boot.loader.tools.Repackager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class WapRunTask extends JavaExec {

    public void run() {

    }

    @Override
    public void exec() {
        Project project = getProject();
        WapExtension byType = project.getExtensions().findByType(WapExtension.class);
        URL url = byType.getMainJar().get();
        classpath(url);
        ArrayList<String> args = new ArrayList<>();
        args.add("--mode=dev");
        args.add("--space="+project.getRootDir().getPath());
        setArgs(args);
        super.exec();
    }
}

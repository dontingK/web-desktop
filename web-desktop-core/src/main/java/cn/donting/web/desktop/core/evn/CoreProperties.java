package cn.donting.web.desktop.core.evn;


import cn.donting.web.desktop.core.annotation.Evn;
import org.springframework.core.env.ConfigurableEnvironment;

public class CoreProperties {
    private static Evn mode;
    private static String space;

    public static void init(ConfigurableEnvironment environment) {
        System.out.println("CoreProperties init.......");
        String mode = environment.getProperty("mode");
        if (mode == null) {
            CoreProperties.mode = Evn.RUN;
        } else {
            CoreProperties.mode = Evn.valueOf(mode.toUpperCase());
        }
        String space = environment.getProperty("space");
        if (space == null) {
            CoreProperties.space = System.getProperty("user.dir");
        } else {
            CoreProperties.space = space;
        }
        System.setProperty("user.space",CoreProperties.space);
    }


    public static String getSpace() {
        return space;
    }

    public static Evn getMode() {
        return mode;
    }

}

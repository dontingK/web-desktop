package cn.donting.web.desktop.sys.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author donting
 * 2021-06-09 下午11:17
 */
@RestController
public class TestController {
    @GetMapping("/test")
    public String test(HttpServletRequest request, HttpSession session){
        HttpSession session1 = request.getSession();
        return "";
    }
}

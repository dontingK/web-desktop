package cn.donting.web.desktop.core.exception;

public class AppNotSupportException extends AppRuntimeException {
    public AppNotSupportException() {
        super("未支持!!!");
    }

    public AppNotSupportException(String message, Throwable cause) {
        super("未支持!!!", cause);
    }
}

package cn.donting.web.desktop.core.evn;

import org.springframework.boot.SpringApplication;
import org.springframework.core.env.CommandLinePropertySource;
import org.springframework.core.env.ConfigurableEnvironment;

public class EnvironmentPostProcessor implements org.springframework.boot.env.EnvironmentPostProcessor{

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        CoreProperties.init(environment);
    }
}

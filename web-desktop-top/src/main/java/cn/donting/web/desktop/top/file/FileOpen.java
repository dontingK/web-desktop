package cn.donting.web.desktop.top.file;


import lombok.Data;

import java.io.File;

@Data
public class FileOpen {
    public  static final String unknown="";
    public  static final String directory=null;

    /**
     * 扩展名
     */
    private String extName;
    /**
     * 显示图标
     * /a.png
     */
    private String icon;
    /**
     * 打开的url
     *  /openFilePng?path=
     */
    private String openFileUrl;

    public FileOpen(String extName, String icon, String openFileUrl) {
        this.extName = extName;
        this.icon = icon;
        this.openFileUrl = openFileUrl;
    }

}

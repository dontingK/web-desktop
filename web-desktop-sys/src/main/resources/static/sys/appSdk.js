let windowId = null;
let event = ["windowFocus", "windowClose"];
console.log(location)
console.log(getQueryStringByName("appId"))
//根据QueryString参数名称获取值
function getQueryStringByName(name) {
    let result = location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
    if (result == null || result.length < 1) {
        return "";
    }
    return result[1];
}

let appId = getQueryStringByName("appId");
self.appId = appId;
//由 desktop 提供
window.windowApiTest = {
    registerEvent: (event, callback) => {
    },
    publishEvent: (event, data) => {
    },
    setTitle: (title) => {
    },
    setIcon: (icon) => {
    },
    openFile: (filePath) => {
    },
    openWindow: (url, title, icon, param) => {
    },
    fileClipboard: {
        type: 'cut/copy/delete',
        files: ['/a.txt']
    },
    close: () => {
    },
    openMenu: (e, menu) => {
        menu = [
            {
                label: "新建文件夹",
                icon: "icon.png",
                enable: () => {
                    return true;
                },
                call: () => {

                }
            },
        ]
    },
    windowParam: {},
    appId: appId,
}

!function () {
    if (window.frameElement) {
        windowId = window.frameElement.id;
        console.log(windowId)
        iniSysApi();
    }

    function iniSysApi() {
        if (window.windowApi) {
            return
        }
        let sysApi = window.top.getWindowApi(windowId);
        window.windowApi = sysApi;
        //回设置 到 windowApi
        console.log(window.windowApi)
        window.windowApi.appId = self.appId;
        window.addEventListener("mousedown", () => {
            window.windowApi.publishEvent("windowFocus");
        }, true)
        sysApi.title="复制"
    }
}()


class AppWebSocket extends WebSocket {
    constructor(url, protocols) {
        console.debug("AppWebSocket")
        super(url, protocols);
    }
}

window.WebSocket = AppWebSocket;
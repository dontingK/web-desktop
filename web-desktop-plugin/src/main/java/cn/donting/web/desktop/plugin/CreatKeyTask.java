package cn.donting.web.desktop.plugin;

import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.internal.tasks.DefaultSourceSet;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.tasks.SourceSet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.jar.JarFile;

public class CreatKeyTask extends DefaultTask {

   public void creat(){
       Project project = getProject();
       String group = (String)project.getGroup();
       String name = project.getName();
       String keyString =  group+":"+ name;
       SourceSet mainSourceSet = project.getConvention().getPlugin(JavaPluginConvention.class).getSourceSets()
               .getByName(SourceSet.MAIN_SOURCE_SET_NAME);
       String resources = mainSourceSet.getResources().getSourceDirectories().getAsPath();
       File key = new File(resources + File.separator + "META-INF" + File.separator + "wapKey.key");
       if(!key.getParentFile().exists()){
           key.getParentFile().mkdirs();
       }

       try {
           if(!key.exists()){
               key.createNewFile();
           }
           FileOutputStream fileOutputStream = new FileOutputStream(key);
           fileOutputStream.write(keyString.getBytes(StandardCharsets.UTF_8));
           fileOutputStream.close();
       } catch (IOException e) {
           e.printStackTrace();
       }
       System.out.println(key);
   }


}

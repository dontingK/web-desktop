package cn.donting.desktop.run.loader;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * @author donting
 * 2021-05-29 下午2:12
 */
public class SysDevClassLoader extends AbsSysClassLoader{
    private String mainClass;

    public SysDevClassLoader(URL[] urls, String mainClass) {
        super(urls);
        this.mainClass = mainClass;
    }

    @Override
    public Class<?> getMainClass() throws ClassNotFoundException {
        Class<?> aClass = loadClass(mainClass);
        return aClass;
    }

    public static AbsSysClassLoader  getInstance() throws Exception{
        File file=new File(System.getProperty("user.dir")+File.separator+"web-desktop-sys.classpath");
        FileInputStream fileInputStream=new FileInputStream(file);
        int available = fileInputStream.available();
        byte[] bytes = new byte[available];
        fileInputStream.read(bytes);
        String s=new String(bytes);
        String[] split = s.split("\n");
        ArrayList<URL> urlArrayList=new ArrayList<>();
        for (int i = 1; i <split.length ; i++) {
            URL url = new File(split[i]).toURL();
            urlArrayList.add(url);
        }
        SysDevClassLoader sysDevClassLoader = new SysDevClassLoader(urlArrayList.toArray(new URL[0]),split[0]);
        return sysDevClassLoader;
    }

    public static void main(String[] args) throws Exception {
        AbsSysClassLoader in = SysDevClassLoader.getInstance();


    }
}

package cn.donting.web.desktop.core.unilt;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class FileUtil {


    public static void copy(File dest, URL url) throws IOException {
        if(!dest.exists()){
            dest.createNewFile();
        }
        FileOutputStream fileOutputStream=new FileOutputStream(dest);
        InputStream inputStream = url.openStream();
        byte[] bytes=new byte[inputStream.available()];
        inputStream.read(bytes);
        fileOutputStream.write(bytes);
        inputStream.close();
        fileOutputStream.close();
    }

}

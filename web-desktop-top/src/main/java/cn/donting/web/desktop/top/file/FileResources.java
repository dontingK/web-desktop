package cn.donting.web.desktop.top.file;

public interface FileResources {
    /**
     * 打开 文件选择器 url
     *
     * @return
     */
    String openFileSelectUrl();
}

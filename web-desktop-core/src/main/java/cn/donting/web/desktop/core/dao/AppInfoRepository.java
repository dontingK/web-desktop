package cn.donting.web.desktop.core.dao;

import cn.donting.web.desktop.core.entity.AppInfo;
import cn.donting.web.desktop.top.AppType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppInfoRepository extends JpaRepository<AppInfo,Long> {
    AppInfo findByFileName(String name);
//    AppInfo findByAppId(Long id);
    AppInfo findFirstByType(AppType  type);
    AppInfo findAppInfoByAppKey(String  appKey);

}

package cn.donting.web.desktop.core.servlet.websocket;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.websocket.common.scopes.WebSocketContainerScope;
import org.eclipse.jetty.websocket.jsr356.server.AnnotatedServerEndpointConfig;

import javax.servlet.http.HttpSession;
import javax.websocket.DeploymentException;
import javax.websocket.Extension;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;
import java.util.Map;

/**
 * @author donting
 * 2021-06-28 下午9:15
 */
@Slf4j
public class AppEndpointConfig extends ServerEndpointConfig.Configurator {
    private final ServerEndpointConfig.Configurator configurator;

    public AppEndpointConfig(ServerEndpointConfig.Configurator configurator) {
        this.configurator = configurator;
    }

    @Override
    public <T> T getEndpointInstance(Class<T> endpointClass) throws InstantiationException {
        return configurator.getEndpointInstance(endpointClass);
    }

    @Override
    public List<Extension> getNegotiatedExtensions(List<Extension> installed, List<Extension> requested) {
        return configurator.getNegotiatedExtensions(installed, requested);
    }

    @Override
    public String getNegotiatedSubprotocol(List<String> supported, List<String> requested) {
        return configurator.getNegotiatedSubprotocol(supported, requested);
    }

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        log.debug("代理....modifyHandshake");
        Map<String, Object> userProperties = sec.getUserProperties();
        HttpSession httpSession = (HttpSession)request.getHttpSession();
        userProperties.put("httpSession",httpSession);
        log.debug("userProperties设置httpSession");
        configurator.modifyHandshake(sec, request, response);

    }

    @Override
    public boolean checkOrigin(String originHeaderValue) {
        return configurator.checkOrigin(originHeaderValue);
    }


}

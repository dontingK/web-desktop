package cn.donting.web.desktop.file.resources;

import cn.donting.web.desktop.top.App;
import cn.donting.web.desktop.top.AppType;
import cn.donting.web.desktop.top.file.FileOpen;
import cn.donting.web.desktop.top.file.FileRegister;
import cn.donting.web.desktop.top.file.FileResources;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@App(name = "资源管理器",version = "0.0.0.1",numberVersion = 1,index = "/index.html",type = AppType.FileResources)
public class WebDesktopFileResourcesApplication implements FileResources , FileRegister {

    public static void main(String[] args) {
        SpringApplication.run(WebDesktopFileResourcesApplication.class, args);
    }

    @Override
    public String openFileSelectUrl() {
        return "/index.html?model=select";
    }

    @Override
    public FileOpen[] register() {
        return new FileOpen[]{new FileOpen(FileOpen.directory,"static/icon/directory.png","/")};
    }
}

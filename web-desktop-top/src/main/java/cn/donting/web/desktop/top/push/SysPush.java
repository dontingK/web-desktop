package cn.donting.web.desktop.top.push;

import cn.donting.web.desktop.top.push.msg.ConfirmMsg;


/**
 * 系统推送到 桌面
 */
public interface SysPush {

    /**
     * 推送到前端 用户确认
     * @return 用户确认结果
     */
    default boolean confirm(ConfirmMsg confirmMsg) {
            return confirm(confirmMsg,5000,false);
    }

    /**
     * 推送到前端 用户确认
     * @param confirmMsg confirmMsg 内容
     * @param timeout 超时毫秒秒 ms
     * @param defaultActive 超时默认行为
     * @return 用户确认结果
     */
    boolean confirm(ConfirmMsg confirmMsg, long timeout, boolean defaultActive);

}

package cn.donting.web.desktop.plugin;

import org.gradle.api.*;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.RegularFile;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.TaskProvider;
import org.springframework.boot.gradle.plugin.ResolveMainClassName;

import java.io.File;
import java.io.IOException;

public class TaskRegister implements Plugin<Project> {
    private static final String sys = "web-desktop-sys";
    Project project;

    @Override
    public void apply(Project project) {

        this.project = project;
        String name = project.getName();
        project.getExtensions().create("wap",WapExtension.class,project);
        wapClasspath();
        if (name.equalsIgnoreCase("web-desktop-sys")) {
            sysJar();
        }else{
            wapKey();
            wapRun();
        }
    }


    public void wapClasspath() {
        TaskProvider<WapClasspathTask> webDesktopClassPath = project.getTasks().register("wapClasspath", WapClasspathTask.class, (buildTask) -> {

        });
        webDesktopClassPath.get().dependsOn("build");
        webDesktopClassPath.get().dependsOn("wapKey");
        webDesktopClassPath.get().setGroup("webapp");
    }


    public void sysJar() {
        TaskProvider<SysJarTask> sysJarTask = project.getTasks().register("sysJar", SysJarTask.class, (task) -> {

            task.doLast(task1 -> {
                try {
                    task.build();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
        sysJarTask.get().dependsOn("bootJar");
        sysJarTask.get().setGroup("webapp");
    }

    public void wapKey() {
        TaskProvider<CreatKeyTask> sysJarTask = project.getTasks().register("wapKey", CreatKeyTask.class, (task) -> {
            task.doLast(task1 -> {
                try {
                    task.creat();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });
        sysJarTask.get().setGroup("webapp");
    }

    public void wapRun(){
        TaskProvider<WapRunTask> sysJarTask = project.getTasks().register("wapRun", WapRunTask.class, (task) -> {
            task.doLast(task1 -> {
                task.run();
            });
        });
        sysJarTask.get().setGroup("webapp");
    }
}

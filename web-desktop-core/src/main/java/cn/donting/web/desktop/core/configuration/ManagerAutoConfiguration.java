package cn.donting.web.desktop.core.configuration;

import cn.donting.web.desktop.core.annotation.Evn;
import cn.donting.web.desktop.core.annotation.EvnCondition;
import cn.donting.web.desktop.core.app.AppManager;
import cn.donting.web.desktop.core.app.DevAppManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class ManagerAutoConfiguration {


    @Bean("appManager")
    @EvnCondition(Evn.DEV)
    public AppManager devAppManager(){
        log.info("load DevAppManager");
        return new DevAppManager();
    }
    @Bean("appManager")
    @ConditionalOnMissingBean(DevAppManager.class)
    public AppManager appManager(){
        log.info("load AppManager");
        return new AppManager();
    }
}

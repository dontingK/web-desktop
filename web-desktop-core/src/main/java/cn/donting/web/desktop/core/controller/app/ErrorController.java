package cn.donting.web.desktop.core.controller.app;

import cn.donting.web.desktop.core.domain.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author donting
 * 2021-06-06 下午4:05
 */
@RestController
@RequestMapping("/sys/app/error")
public class ErrorController {

    @RequestMapping("/requestAppId")
    public ResponseEntity<String> requestAppId(String msg) {
        ResponseEntity error = ResponseEntity.error(1404, msg);
        return error;
    }

    @RequestMapping("/appRunning")
    public ResponseEntity<String> appRunning(String msg) {
        ResponseEntity error = ResponseEntity.error(1501, msg);
        return error;
    }
    @RequestMapping()
    public ResponseEntity<String> errorMsg(String msg) {
        ResponseEntity error = ResponseEntity.error(500, msg);
        return error;
    }

}

package cn.donting.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

//@Slf4j
public class FileMoveTask extends FileCopyTask {
    private static final Log log = LogFactory.get();

    public FileMoveTask(FileAction fileAction) {
        super(fileAction,true);
    }

}

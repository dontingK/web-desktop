package cn.donting.web.desktop.core.app;

import cn.donting.web.desktop.servlet.ThreadContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;

/**
 * 不带appId 默认 sys
 * @author donting
 * 配合 AppIdFilter
 * @see cn.donting.web.desktop.core.servlet.AppIdFilter
 * 2021-06-06 下午3:34
 */
@Service
@Slf4j
public class DefaultAppGetting implements AppGetting {

    @Override
    public Long getAppId(ServletRequest servletRequest) {
        Object attribute = servletRequest.getAttribute(AppGetting.REQUEST_APP_ID);
        String appId = (String) attribute;

        try {
            long l = Long.parseLong(appId);
            ThreadContext.setAppId(l);
            return l;
        } catch (Exception e) {
            log.warn(appId + "not parseLong" + e.getMessage());
        }
        return null;
    }
}

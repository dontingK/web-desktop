package cn.donting.web.desktop.core.loader;

import org.springframework.boot.loader.JarLauncher;
import org.springframework.boot.loader.archive.Archive;

import java.net.URL;
import java.util.Iterator;


public class BootLauncher extends JarLauncher {

    public BootLauncher(Archive  jarFileArchive) {
        super(jarFileArchive);
    }


    public AppURLClassLoader createAppClassLoader() throws Exception {
        Iterator<Archive> classPathArchivesIterator = getClassPathArchivesIterator();
        AppURLClassLoader classLoader = (AppURLClassLoader)createClassLoader(classPathArchivesIterator);
        return classLoader;
    }

    @Override
    protected AppURLClassLoader createClassLoader(URL[] urls) throws Exception {
        return new BootLauncherURLClassLoader(isExploded(), getArchive(), urls, getClass().getClassLoader());
    }

    @Override
    public String getMainClass() throws Exception {
        return super.getMainClass();
    }


    @Override
    protected boolean isNestedArchive(Archive.Entry entry) {
        return super.isNestedArchive(entry);
    }

    @Override
    public void launch(String[] args) throws Exception {
        super.launch(args);
    }
}
